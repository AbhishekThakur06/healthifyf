package api;

/**
 * Created by Abhishek on 12/4/2016.
 */
        import model.SlotData;
        import retrofit2.Call;
        import retrofit2.http.GET;
        import retrofit2.http.Path;
        import retrofit2.http.Query;


public interface ApiInterface {
    @GET("booking/slots/{id}")
    Call<SlotData> getSlots(@Path("id") String id, @Query("username") String userName, @Query("api_key") String apiKey, @Query("vc") int vc, @Query("expert_username") String expertUserName, @Query("format") String format);
}
