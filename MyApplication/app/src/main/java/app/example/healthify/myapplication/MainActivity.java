package app.example.healthify.myapplication;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;


import api.ApiClient;
import api.ApiInterface;
import fragments.ExpandableExampleFragment;
import model.DateSlots;
import model.SlotData;
import model.SlotStructure;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utility.DateUtils;

public class MainActivity extends AppCompatActivity {
    TimeTablePagerAdapter mDemoCollectionPagerAdapter;
    ViewPager mViewPager;
    private static final String API_KEY = "a4aeb4e27f27b5786828f6cdf00d8d2cb44fe6d7";
    private static final String USER_NAME = "alok@x.coz";
    private static final String EXPERT_USER_NAME = "neha@healthifyme.com";
    private static final int VC = 276;
    private TabLayout tabLayout;
    private ProgressBar progress;
    private List<DateSlots> slots = new ArrayList<DateSlots>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDemoCollectionPagerAdapter =
                new TimeTablePagerAdapter(
                        getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tab);
        progress = (ProgressBar) findViewById(R.id.progressBar);
        mViewPager.setAdapter(mDemoCollectionPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        progress.setVisibility(View.GONE);
    }

    private void makeNetworkRequest() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<SlotData> call = apiService.getSlots("all",USER_NAME,API_KEY,VC,EXPERT_USER_NAME,"json");
        call.enqueue(new Callback<SlotData>() {
            @Override
            public void onResponse(Call<SlotData>call, Response<SlotData> response) {
               HashMap<String, SlotStructure> slotMap= response.body().getSlots();
                Set<String>  dates = slotMap.keySet();
                slots.clear();
                for(String date : dates) {
                    DateSlots dateSlots = new DateSlots();
                    dateSlots.setDate(date);
                    dateSlots.setSlotStructure(slotMap.get(date));
                    String pageTitle = DateUtils.convertDate(date,DateUtils.formatDate2);
                    dateSlots.setPageTitle(pageTitle);
                    slots.add(dateSlots);
                }

              runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                      progress.setVisibility(View.GONE);
                      mDemoCollectionPagerAdapter.notifyDataSetChanged();
                  }
              });

                //Log.d(TAG, "Number of movies received: " + movies.size());
            }


            @Override
            public void onFailure(Call<SlotData>call, Throwable t) {
                // Log error here since request failed
               // Log.e(TAG, t.toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        progress.setVisibility(View.VISIBLE);
        makeNetworkRequest();
    }

    public class TimeTablePagerAdapter extends FragmentStatePagerAdapter {

        public TimeTablePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new ExpandableExampleFragment();
            Bundle args = new Bundle();
            // Our object is just an integer :-P
            args.putParcelable(ExpandableExampleFragment.ARG_OBJECT, slots.get(i));
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return slots.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return slots.get(position).getPageTitle();
        }
        public void refreshData() {

        }


    }
}
