package utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Abhishek on 12/5/2016.
 */

public class DateUtils {
    public static final String formatDate2 = "yyyy-MM-dd";
    public static final String formatDate3 = "yyyy-MM-dd hh:mm:s+00:00";
    public static long getDateInLongFromFormatTwo(String date, String format ) {

        SimpleDateFormat dateFormat=new SimpleDateFormat(format);
        Date date1 = null;
        try {
            date1 = dateFormat.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (date1 == null) {
            return 0;
        }
        else{
            return date1.getTime();
        }
    }
    public static String convertDate(String strDate , String format) {
        try {
            long date = getDateInLongFromFormatTwo(strDate, format);
            Calendar cal1 = Calendar.getInstance();
            // cal1.setFirstDayOfWeek(Calendar.MONDAY);
            cal1.setTime(new Date(date));
            int day = cal1.get(Calendar.DAY_OF_WEEK);
            String dayName = new SimpleDateFormat("EEE", Locale.ENGLISH).format(date);
            return day + "\n" + dayName;
        } catch (Exception e){
            e.printStackTrace();
        }
        return  null ;
    }
    public static String convertDate2(String strDate , String format) {
        try {
            long date = getDateInLongFromFormatTwo(strDate, formatDate3);
            String time = new SimpleDateFormat("HH:mm a", Locale.ENGLISH).format(date);
            return time;
        } catch (Exception e){
            e.printStackTrace();
        }
        return  null ;
    }
}
