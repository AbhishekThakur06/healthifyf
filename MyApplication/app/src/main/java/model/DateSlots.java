package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Abhishek on 12/5/2016.
 */

public class DateSlots implements Parcelable {
    private String date;
    private SlotStructure slotStructure;
    private String pageTitle;
    public DateSlots() {

    }
    public DateSlots(Parcel in) {
        date = in.readString();
        slotStructure = in.readParcelable(SlotStructure.class.getClassLoader());
    }

    public static final Creator<DateSlots> CREATOR = new Creator<DateSlots>() {
        @Override
        public DateSlots createFromParcel(Parcel in) {
            return new DateSlots(in);
        }

        @Override
        public DateSlots[] newArray(int size) {
            return new DateSlots[size];
        }
    };

    public SlotStructure getSlotStructure() {
        return slotStructure;
    }

    public void setSlotStructure(SlotStructure slotStructure) {
        this.slotStructure = slotStructure;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(date);
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
}
