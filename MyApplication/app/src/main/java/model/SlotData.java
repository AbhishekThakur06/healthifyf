package model;

/**
 * Created by Abhishek on 12/5/2016.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class SlotData {

    @SerializedName("slots")
    @Expose
    private HashMap<String, SlotStructure> slots;

    public HashMap<String, SlotStructure> getSlots() {
        return slots;
    }

    public void setSlots(HashMap<String, SlotStructure> slots) {
        this.slots = slots;
    }
    //private Slots slots;


/*
    public Slots getSlots() {
        return slots;
    }

     public void setSlots(Slots slots) {
        this.slots = slots;
    }*/

}
