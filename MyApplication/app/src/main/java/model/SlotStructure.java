package model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Abhishek on 12/5/2016.
 */

        import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class SlotStructure implements Parcelable {

    @SerializedName("afternoon")
    @Expose
    private List<SlotDetails> afternoon = null;
    @SerializedName("evening")
    @Expose
    private List<SlotDetails> evening = null;
    @SerializedName("morning")
    @Expose
    private List<SlotDetails> morning = null;

    public SlotStructure(Parcel in) {
        this.afternoon = in.readArrayList(null);
        this.morning = in.readArrayList(null);
        this.evening = in.readArrayList(null);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(afternoon);
        dest.writeList(evening);
        dest.writeList(morning);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SlotStructure> CREATOR = new Creator<SlotStructure>() {
        @Override
        public SlotStructure createFromParcel(Parcel in) {
            return new SlotStructure(in);
        }

        @Override
        public SlotStructure[] newArray(int size) {
            return new SlotStructure[size];
        }
    };

    /**
     *
     * @return
     *     The afternoon
     */
    public List<SlotDetails> getAfternoon() {
        return afternoon;
    }

    /**
     *
     * @param afternoon
     *     The afternoon
     */
    public void setAfternoon(List<SlotDetails> afternoon) {
        this.afternoon = afternoon;
    }

    /**
     *
     * @return
     *     The evening
     */
    public List<SlotDetails> getEvening() {
        return evening;
    }

    /**
     *
     * @param evening
     *     The evening
     */
    public void setEvening(List<SlotDetails> evening) {
        this.evening = evening;
    }

    /**
     *
     * @return
     *     The morning
     */
    public List<SlotDetails> getMorning() {
        return morning;
    }

    /**
     *
     * @param morning
     *     The morning
     */
    public void setMorning(List<SlotDetails> morning) {
        this.morning = morning;
    }

}
