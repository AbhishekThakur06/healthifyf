
package model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SlotDetails implements Parcelable {

    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("is_booked")
    @Expose
    private Boolean isBooked;
    @SerializedName("is_expired")
    @Expose
    private Boolean isExpired;
    @SerializedName("slot_id")
    @Expose
    private int slotId;
    @SerializedName("start_time")
    @Expose
    private String startTime;

    protected SlotDetails(Parcel in) {
        endTime = in.readString();
        startTime = in.readString();
        slotId = in.readInt();
    }

    public static final Creator<SlotDetails> CREATOR = new Creator<SlotDetails>() {
        @Override
        public SlotDetails createFromParcel(Parcel in) {
            return new SlotDetails(in);
        }

        @Override
        public SlotDetails[] newArray(int size) {
            return new SlotDetails[size];
        }
    };

    /**
     * 
     * @return
     *     The endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * 
     * @param endTime
     *     The end_time
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * 
     * @return
     *     The isBooked
     */
    public Boolean getIsBooked() {
        return isBooked;
    }

    /**
     * 
     * @param isBooked
     *     The is_booked
     */
    public void setIsBooked(Boolean isBooked) {
        this.isBooked = isBooked;
    }

    /**
     * 
     * @return
     *     The isExpired
     */
    public Boolean getIsExpired() {
        return isExpired;
    }

    /**
     * 
     * @param isExpired
     *     The is_expired
     */
    public void setIsExpired(Boolean isExpired) {
        this.isExpired = isExpired;
    }

    /**
     * 
     * @return
     *     The slotId
     */
    public Integer getSlotId() {
        return slotId;
    }

    /**
     * 
     * @param slotId
     *     The slot_id
     */
    public void setSlotId(Integer slotId) {
        this.slotId = slotId;
    }

    /**
     * 
     * @return
     *     The startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * 
     * @param startTime
     *     The start_time
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(endTime);
        parcel.writeString(startTime);
        parcel.writeInt(slotId);
    }
}
