package adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import java.util.List;

import app.example.healthify.myapplication.R;
import model.DateSlots;
import model.SlotData;
import model.SlotDetails;
import utility.DateUtils;

public class ExpandableExampleAdapter
        extends AbstractExpandableItemAdapter<ExpandableExampleAdapter.MyGroupViewHolder, ExpandableExampleAdapter.MyChildViewHolder> {
    private static final String TAG = "MyExpandableItemAdapter";

    // NOTE: Make accessible with short name
    private interface Expandable extends ExpandableItemConstants {
    }

    private DateSlots mProvider;

    public static abstract class MyBaseViewHolder extends AbstractExpandableItemViewHolder {
        public FrameLayout mContainer;
        public TextView mTextView;

        public MyBaseViewHolder(View v) {
            super(v);
            mContainer = (FrameLayout) v.findViewById(R.id.container);
            mTextView = (TextView) v.findViewById(android.R.id.text1);
        }
    }

    public static class MyGroupViewHolder extends MyBaseViewHolder {
        public ImageView mIndicator;
        public TextView availableSlots;

        public MyGroupViewHolder(View v) {
            super(v);
            mIndicator = (ImageView) v.findViewById(R.id.indicator);
            availableSlots = (TextView) v.findViewById(R.id.available_slots);
        }
    }

    public static class MyChildViewHolder extends MyBaseViewHolder {
        public MyChildViewHolder(View v) {
            super(v);

        }
    }

    public ExpandableExampleAdapter(DateSlots dataProvider) {
        mProvider = dataProvider;

        // ExpandableItemAdapter requires stable ID, and also
        // have to implement the getGroupItemId()/getChildItemId() methods appropriately.
        setHasStableIds(true);
    }

    @Override
    public int getGroupCount() {
        return 3;
    }

    @Override
    public int getChildCount(int groupPosition) {
        List<SlotDetails> slotDetails = mapSessionWithId(groupPosition);
        if (slotDetails != null) {
            return slotDetails.size();
        }
        return 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;//mProvider.getGroupItem(groupPosition).getGroupId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;//mProvider.getChildItem(groupPosition, childPosition).getChildId();
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public MyGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_group_item, parent, false);
        return new MyGroupViewHolder(v);
    }

    @Override
    public MyChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.list_item, parent, false);
        return new MyChildViewHolder(v);
    }

    @Override
    public void onBindGroupViewHolder(MyGroupViewHolder holder, int groupPosition, int viewType) {
        // child item
        String item = getGroupName(groupPosition);
        int numberOfSlots = 0;
        if (mapSessionWithId(groupPosition) != null)
            numberOfSlots = mapSessionWithId(groupPosition).size();
        // set text
        holder.mTextView.setText(item);
        holder.availableSlots.setText(numberOfSlots + " available slots");
        // mark as clickable
        holder.itemView.setClickable(true);


        // set background resource (target view ID: container)
        final int expandState = holder.getExpandStateFlags();

        if ((expandState & ExpandableItemConstants.STATE_FLAG_IS_UPDATED) != 0) {
            int bgResId;
            boolean isExpanded;
            boolean animateIndicator = ((expandState & Expandable.STATE_FLAG_HAS_EXPANDED_STATE_CHANGED) != 0);

            if ((expandState & Expandable.STATE_FLAG_IS_EXPANDED) != 0) {
                // bgResId = R.drawable.bg_group_item_expanded_state;
                isExpanded = true;
            } else {
                //  bgResId = R.drawable.bg_group_item_normal_state;
                isExpanded = false;
            }

            // holder.mContainer.setBackgroundResource(bgResId);
            //holder.mIndicator.setExpandedState(isExpanded, animateIndicator);
        }
    }

    @Override
    public void onBindChildViewHolder(MyChildViewHolder holder, int groupPosition, int childPosition, int viewType) {
        // group item
        try {
            final SlotDetails item = mapSessionWithId(groupPosition).get(childPosition); //mProvider.getChildItem(groupPosition, childPosition);

            // set text
            holder.mTextView.setText(DateUtils.convertDate2(item.getStartTime(), DateUtils.formatDate3) + " - " + DateUtils.convertDate2(item.getEndTime(), DateUtils.formatDate3));

            // set background resource (target view ID: container)
            int bgResId;
            // bgResId = R.drawable.bg_item_normal_state;
            //holder.mContainer.setBackgroundResource(bgResId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(MyGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        // check the item is *not* pinned
       /* if (mProvider.getGroupItem(groupPosition).isPinned()) {
            // return false to raise View.OnClickListener#onClick() event
            return false;
        }

*/        // check is enabled
        if (!(holder.itemView.isEnabled() && holder.itemView.isClickable())) {
            return false;
        }

        return true;
    }

    private List<SlotDetails> mapSessionWithId(int groupPosition) {
        if (groupPosition == 0) {
            if (mProvider.getSlotStructure().getMorning() != null) {
                return mProvider.getSlotStructure().getMorning();
            }
        } else if (groupPosition == 1) {
            if (mProvider.getSlotStructure().getAfternoon() != null) {
                return mProvider.getSlotStructure().getAfternoon();
            }
        } else if (groupPosition == 2) {
            if (mProvider.getSlotStructure().getEvening() != null) {
                return mProvider.getSlotStructure().getEvening();
            }
        }
        return null;
    }

    private String getGroupName(int groupPosition) {
        if (groupPosition == 0) {
            if (mProvider.getSlotStructure().getMorning() != null) {
                return "MORNING";
            }
        } else if (groupPosition == 1) {
            if (mProvider.getSlotStructure().getAfternoon() != null) {
                return "AFTERNOON";
            }
        } else if (groupPosition == 2) {
            if (mProvider.getSlotStructure().getEvening() != null) {
                return "EVENING";
            }
        }
        return null;
    }
}
