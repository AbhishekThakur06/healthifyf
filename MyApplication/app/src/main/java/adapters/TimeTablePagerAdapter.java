package adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import fragments.ExpandableExampleFragment;

/**
 * Created by Abhishek on 12/4/2016.
 */

public class TimeTablePagerAdapter extends FragmentStatePagerAdapter {

    public TimeTablePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = new ExpandableExampleFragment();
        Bundle args = new Bundle();
        // Our object is just an integer :-P
        args.putInt(ExpandableExampleFragment.ARG_OBJECT, i + 1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "OBJECT " + (position + 1);
    }
}